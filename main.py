# coding=UTF-8

if __name__ == "__main__":

    # Setting by yout score rule.
    score_level = ('A', 'B', 'C', 'D')
    threshold = (80, 70, 60)

    # input formet
    # '空格'均為 '\t' 請注意!!
    '''
    1	1041113	陳郁翔	56
    2	1060425	鄭善峰	64
    ..
    ..
    56	1083535	李美文	82 
    '''

    f = open("./score.txt", "r", encoding="utf-8")
    lines = f.readlines()
    f.close()
    
    dict_score={e[1]: {"seat_idx:":e[0],"name":e[2], "score":e[3]} for e in [line.strip().split('\t') for line in lines]}

    for e in dict_score:
        # dict_score[e] is <dict>
        score = int(dict_score[e]["score"])

        if score >= threshold[0]:
            dict_score[e].update({"score_level":score_level[0]})
        elif score >= threshold[1]:
            dict_score[e].update({"score_level":score_level[1]})
        elif score >= threshold[2]:
            dict_score[e].update({"score_level":score_level[2]})
        elif score < threshold[2] and score >= 0:
            dict_score[e].update({"score_level":score_level[3]})
        else:
            raise BaseException("ERROR: score error! \n...\t\t score="+ str(score))

    # validation, Check everyone have "score_level" !
    for student_id in dict_score:
        value = dict_score[student_id].get("score_level")
        if value == None:
            raise "ERROR: No assign any score_level."
        if value not in score_level:
            raise BaseException("ERROR: Wrong score_level. \n...\t\tscore_level=" + str(value))
    # ====== validation pass ======

    # 生成檔案
    f = open("./command.txt", "w", encoding="utf-8")
    # 生成 html dom 語法
    for student_id in dict_score:
        a = '<!-- '+ dict_score[student_id]["name"]+", "+dict_score[student_id]["score"] +' -->'
        b = "document.getElementById(\'" + student_id + "\').value = \'" + \
            dict_score[student_id]["score_level"] + "\'"
        f.write(a+'\n')
        f.write(b+'\n\n')
    f.close()
